var mongoose = require('mongoose');

var shirtSchema = mongoose.Schema({
    shirt : {
        code        : Number,
        createdDate : {type: Date, default: Date.now},
    }
});

shirtSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

shirtSchema.methods.verifyPassword = function(password) {
    return bcrypt.compareSync(password, this.user.password);
};

shirtSchema.methods.updateUser = function(request, response){

    this.user.name = request.body.name;
    this.user.address = request.body.address;
    this.user.save();
    response.redirect('/user');
};


module.exports = mongoose.model('Shirt', shirtSchema);