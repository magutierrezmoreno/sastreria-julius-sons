module.exports = function(app) {

    app.get('/', function (req, res) {
        var data = {
            title: 'Página principal'
        };
        res.render('index', data);
        //res.send('Hello World!');
    });


// This responds a POST request for the homepage
    app.post('/', function (req, res) {
        console.log("Got a POST request for the homepage");
        res.send('Hello POST');
    });
    // Registro de usuario
    app.get('/signup', function(request, response) {
        response.render('signup');
    });
// This responds a DELETE request for the /del_user page.
    app.delete('/del_user', function (req, res) {
        console.log("Got a DELETE request for /del_user");
        res.send('Hello DELETE');
    })

// This responds a GET request for the /list_user page.
    app.get('/list_user', function (req, res) {
        console.log("Got a GET request for /list_user");
        res.send('Page Listing');
    })

// This responds a GET request for abcd, abxcd, ab123cd, and so on
    app.get('/ab*cd', function (req, res) {
        console.log("Got a GET request for /ab*cd");
        res.send('Page Pattern Match');
    })

};