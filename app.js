var express = require('express'),
    swig = require('swig'),
    mongoose =  require('mongoose');

var port = process.env.PORT || 8080;
var app = express();
var configDB = require('./config/database.js');

// Conexión de MongoDB
mongoose.connect(configDB.url);


app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', __dirname+'/views');


require('./app/routes.js')(app);

var server = app.listen(port, function () {

    var host = server.address().address
    var port = server.address().port

    console.log('Example app listening at http://%s:%s', host, port)

});